/*
Universidade Federal de São Carlos - Campus Sorocaba
Técnicas de Desenvolvimento de Software
Aluno ~ João Victor Giraldez Pereira Vilar Silva / 759512

Programa para calcular a média de n (n<=256) números de ponto flutuante.

gcc -c gera o arquivo objeto
*/

#include <stdio.h>
#include <stdlib.h>
#include "estatistica.h"

int main(){
    printf("Este programa irá calcular a média de n números de ponto flutuante\n");
    int n,i;
    double* arr;
    double avg=0;

    scanf("%d",&n);
    if (n > 256)
        printf("Excedeu o valor de entrada.\nReinicie o programa");

    arr = (double*) malloc(n*sizeof(double));
    for(i=0;i<n;i++){
        scanf("%lf",&arr[i]);
    }
    printf("Memória utilizada: %ld bytes\n",n*sizeof(double));
    avg = media(n,arr);
    free(arr);

    printf("%.2f\n",avg);


    return 0;
}