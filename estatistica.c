#include <stdio.h>
#include "estatistica.h"

double media(int n, double *v){
    int i;
    double avg = 0;

    for(i=0;i<n;i++)
        avg += v[i];
    avg /= n;

    return avg;
    }
