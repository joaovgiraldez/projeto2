all: media clean run

media: estatistica.o
	gcc estatistica.o media.c -o main

estatistica.o: estatistica.h
	gcc -c estatistica.c

clean:
	rm -rf *.0

deepclean:
	rm main
run:
	./main